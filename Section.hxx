#ifndef SECTION_HXX
#define SECTION_HXX

#include <string>
#include <boost/algorithm/string.hpp>
#include "Entries.hxx"
#include "Getter.hxx"

class Section : public Getter<std::string, Entries &>
{
public:
  Section();
  Section(const std::string, const Entries &);
  Section(const Section &);
  ~Section();
  Section &operator=(const Section &);
  void set(const std::string, const Entries &);
  void setKey(const std::string);
  void setValue(const Entries &);
  std::string getKey() const;
  Entries &getValue() const;
private:
  std::string key;
  Entries *value;
};

#endif
