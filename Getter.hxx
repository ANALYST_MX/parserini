#ifndef GETTER_HXX
#define GETTER_HXX

template < typename K, typename V > 
class Getter
{
public:
  virtual ~Getter() {};
  virtual K getKey() const = 0;
  virtual V getValue() const = 0;
};

#endif
