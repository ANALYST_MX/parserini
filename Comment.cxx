#include "Comment.hxx"

bool Comment::isComment(const std::string &str)
{
  return str[0] == '\n' || str[0] == '\r' || str[0] == ';';
}
