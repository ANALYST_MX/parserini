GXX := g++
GXXFLAG := -std=c++11 -Wall -g
MAIN := main
TEST := test
CXX := .cxx
HXX := .hxx
OBJ := .o
OBJ_LIST := Getter$(OBJ) Pair$(OBJ) Entry$(OBJ) Entries$(OBJ) Section$(OBJ) Data$(OBJ) ParserINI$(OBJ) Comment$(OBJ)
all: $(MAIN) $(TEST)
$(MAIN): $(MAIN)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(MAIN)$(OBJ) $(OBJ_LIST) -o $(MAIN)
$(MAIN)$(OBJ): $(MAIN)$(CXX) $(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) -c $(MAIN)$(CXX) -o $(MAIN)$(OBJ)
$(TEST): $(TEST)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(TEST)$(OBJ) $(OBJ_LIST) -o $(TEST)
$(TEST)$(OBJ): $(TEST)$(CXX) $(TEST)$(HXX)
	$(GXX) $(GXXFLAG) -c $(TEST)$(CXX) -o $(TEST)$(OBJ)
Getter$(OBJ): Getter$(CXX) Getter$(HXX)
	$(GXX) $(GXXFLAG) -c Getter$(CXX) -o Getter$(OBJ)
Pair$(OBJ): Pair$(CXX) Pair$(HXX)
	$(GXX) $(GXXFLAG) -c Pair$(CXX) -o Pair$(OBJ)
Entry$(OBJ): Entry$(CXX) Entry$(HXX)
	$(GXX) $(GXXFLAG) -c Entry$(CXX) -o Entry$(OBJ)
Entries$(OBJ): Entries$(CXX) Entries$(HXX)
	$(GXX) $(GXXFLAG) -c Entries$(CXX) -o Entries$(OBJ)
Section$(OBJ): Section$(CXX) Section$(HXX)
	$(GXX) $(GXXFLAG) -c Section$(CXX) -o Section$(OBJ)
Data$(OBJ): Data$(CXX) Data$(HXX)
	$(GXX) $(GXXFLAG) -c Data$(CXX) -o Data$(OBJ)
ParserINI$(OBJ): ParserINI$(CXX) ParserINI$(HXX)
	$(GXX) $(GXXFLAG) -c ParserINI$(CXX) -o ParserINI$(OBJ)
Comment$(OBJ): Comment$(CXX) Comment$(HXX)
	$(GXX) $(GXXFLAG) -c Comment$(CXX) -o Comment$(OBJ)
.PHONY: clean check
clean:
	rm -rf $(MAIN) $(TEST) *$(OBJ) *~
check:
	./$(TEST)
