#ifndef COMMENT_HXX
#define COMMENT_HXX

#include <string>

class Comment
{
public:
  static bool isComment(const std::string &);
};

#endif
