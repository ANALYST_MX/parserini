#ifndef PAIR_HXX
#define PAIR_HXX

#include <string>
#include <boost/algorithm/string.hpp>
#include "Getter.hxx"

class Pair : public Getter<std::string, std::string>
{
public:
  Pair(const std::string, const std::string = std::string());
  Pair(const Pair &);
  ~Pair();
  Pair &operator=(const Pair &);
  bool operator==(const Pair &) const;
  bool operator!=(const Pair &) const;
  std::string getKey() const;
  std::string getValue() const;
  void setValue(const std::string);
protected:
  void setKey(const std::string);
private:
  std::string key;
  std::string value;
};

#endif
