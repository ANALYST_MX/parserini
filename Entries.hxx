#ifndef ENTRIES_HXX
#define ENTRIES_HXX

#include <string>
#include <forward_list>
#include <iterator>
#include "Entry.hxx"
#include "Pair.hxx"

class Entries : public Entry, public std::iterator<std::forward_iterator_tag, Pair>
{
public:
  Entries();
  Entries(const Pair &);
  Entries(const Entry &);
  Entries(const std::string, const std::string = std::string());
  ~Entries();
  std::forward_list<Pair *>::iterator begin(); 
  std::forward_list<Pair *>::const_iterator begin() const; 
  std::forward_list<Pair *>::iterator end();
  std::forward_list<Pair *>::const_iterator end() const;
  Entries &operator=(const Entries &);
};

#endif
