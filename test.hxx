#ifndef TEST_HXX
#define TEST_HXX

#include <iostream>
#include <cassert>
#include <string>
#include "Pair.hxx"
#include "Entry.hxx"
#include "Entries.hxx"
#include "Section.hxx"
#include "Data.hxx"
#include "ParserINI.hxx"
#include "Comment.hxx"

void assertPair();
void assertEntries();
void assertSection();
void assertData();
void assertParserINI();
void assertComment();
void ok();
void info(const char *);

#endif
