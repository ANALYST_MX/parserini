#include "Section.hxx"

Section::Section()
  : value(nullptr)
{
}


Section::Section(const std::string key, const Entries &value)
  : value(nullptr)
{
  set(key, value);
}

Section::Section(const Section &other)
  : value(nullptr)
{
  if ( this != &other )
    {
      set(other.key, *other.value);
    }
}

Section::~Section()
{
  if ( value != nullptr )
    {
      delete value;
    }
}

Section &Section::operator=(const Section &other)
{
  set(other.key, *other.value);

  return *this;
}

void Section::set(const std::string newKey, const Entries &newValue)
{
  setKey(newKey);
  setValue(newValue);
}

void Section::setKey(const std::string newKey)
{
  key = newKey;
  boost::algorithm::trim(key);
}

void Section::setValue(const Entries &newValue)
{
  if (value == nullptr)
    {
      value = new Entries;
    }

  *value = newValue;
}

std::string Section::getKey() const
{
  return key;
}

Entries &Section::getValue() const
{
  return *value;
}
