#include "main.hxx"

int main(const int argc, const char *argv[])
{
  if ( argc != 4 )
    {
      std::cout << "Usage: " << argv[0] << " <file.ini> <section> <parameter>" << std::endl;
      return 0;
    }

  std::ifstream in(argv[1]);
  if ( !in )
    {
      std::cout << "Can't open file \"" << argv[1] << '\"' << std::endl;
      return 1;
    }

  std::vector<std::string> lns;

  std::string s;
  while( !in.eof() )
    {
      std::getline(in, s);
      boost::algorithm::trim(s);
      lns.push_back(s.append("\r\n"));
    }
  //lns.erase(remove_if(lns.begin(), lns.end(), std::is_comment()), lns.end());
  std::string text = std::accumulate(lns.begin(), lns.end(), std::string());
   
  return 0;
}
