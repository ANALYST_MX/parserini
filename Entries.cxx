#include "Entries.hxx"

Entries::Entries()
  : Entry()
{
}

Entries::Entries(const Pair &p)
  : Entry(p)
{
}

Entries::Entries(const Entry &other)
  : Entry(other)
{
}

Entries::Entries(const std::string key, const std::string value)
  : Entry(key, value)
{
}

Entries::~Entries()
{
}

std::forward_list<Pair *>::iterator Entries::begin()
{
  return container.begin();
}

std::forward_list<Pair *>::const_iterator Entries::begin() const
{
  return container.begin();
}

std::forward_list<Pair *>::iterator Entries::end()
{
  return container.end();
}

std::forward_list<Pair *>::const_iterator Entries::end() const
{
  return container.end();
}

Entries &Entries::operator=(const Entries &other)
{
  if ( this != &other && &other != nullptr )
    {
      Entry::clear();
      container.assign(other.container.begin(), other.container.end());
   }
      
  return *this;
}
