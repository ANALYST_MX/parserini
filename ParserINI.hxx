#ifndef PARSER_INI_HXX
#define PARSER_INI_HXX

//#include <boost/spirit/include/classic.hpp>
//#include <boost/spirit/include/qi_grammar.hpp>
#include "Data.hxx"

class ParserINI
{
public:
  ParserINI(const Data &);
private:
  Data data;
};

#endif
