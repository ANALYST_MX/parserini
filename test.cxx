#include "test.hxx"

int main(const int argc, const char *argv[])
{
  assertPair();
  assertEntries();
  assertSection();
  assertData();
  assertParserINI();
  assertComment();
  
  return 0;
}

void assertPair()
{
  std::cout << "\t\033[1;34mstart check Pair\033[0m" << std::endl;
  Pair p1("k1", "v1");
  Pair p2("k2", "v2");
  Pair p3(p1);
  Pair p4("\tk4 \r\n");
  info("check operator ==");
  assert(p1 == p3);
  ok();
  info("check operator !=");
  assert(p1 != p2);
  ok();
  info("check get");
  assert(p1.getKey() == "k1");
  assert(p1.getValue() == "v1");
  ok();
  info("check set key");
  assert(p4.getKey() == "k4");
  ok();
  std::cout << "\t\033[1;34mend check Pair\033[0m" << std::endl;
}

void assertEntries()
{
  std::cout << "\t\033[1;34mstart check Entries\033[0m" << std::endl;
  int i = 0;
  bool check = false;
  Pair p1("k1", "v1");
  Entry e1("k2", "v2");
  Entries es1;
  Entries es2("k3", "v3");
  Entries es3(p1);
  es3.add("k4", "v4");
  Entries es4(e1);
  es4.add("k5", "v5");
  Entries es5;
  es5 = es4;
  info("check empty constructor");
  for ( auto it = es1.begin(); it != es1.end(); ++it, ++i );
  assert(i == 0);
  i = 0;
  ok();
  info("check k&v constructor");
  for ( auto it = es2.begin(); it != es2.end(); ++it, ++i );
  assert(i == 1);
  i = 0;
  ok();
  info("check Pair constructor");
  for ( auto it = es3.begin(); it != es3.end(); ++it, ++i );
  assert(i == 2);
  i = 0;
  ok();
  info("check Entry constructor");
  for ( auto it = es4.begin(); it != es4.end(); ++it, ++i );
  assert(i == 2);
  i = 0;
  ok();
  info("check add method");
  es5.add("k6", "v6");
  es5.add(p1);
  for ( auto it = es5.begin(); it != es5.end(); ++it, ++i );
  assert(i == 4);
  i = 0;
  ok();
  info("check update method");
  es5.update("k6", "new value");
  for ( auto it = es5.begin(); it != es5.end(); ++it )
    {
      if ( (*it)->getKey() == "k6" && (*it)->getValue() == "new value" )
	{
	  check = true;
	}
    }
  assert(check);
  check = false;
  ok();
  std::cout << "\t\033[1;34mend check Entries\033[0m" << std::endl;
}

void assertSection()
{
  std::cout << "\t\033[1;34mstart check Section\033[0m" << std::endl;
  Entries es1("k1", "v1");
  Section p1("es1", es1);
  info("check set key");
  assert(p1.getKey() == "es1");
  ok();
  info("check set entry key");
  assert((*p1.getValue().begin())->getKey() == "k1");
  ok();
  info("check set entry value");
  assert((*p1.getValue().begin())->getValue() == "v1");
  ok();
  std::cout << "\t\033[1;34mend check Section\033[0m" << std::endl;
}

void assertData()
{
  std::cout << "\t\033[1;34mstart check Data\033[0m" << std::endl;
  Entries es1("k1", "v1");
  Section p1("es1", es1);
  Section p2;
  info("check operator=");
  p2 = p1;
  assert((*p1.getValue().begin())->getKey() == (*p2.getValue().begin())->getKey());
  ok();
  std::cout << "\t\033[1;34mend check Data\033[0m" << std::endl;
}

void assertParserINI()
{
  std::cout << "\t\033[1;34mstart check ParserINI\033[0m" << std::endl;
  std::cout << "\t\033[1;34mend check ParserINI\033[0m" << std::endl;
}

void assertComment()
{
  std::cout << "\t\033[1;34mstart check Comment\033[0m" << std::endl;
  info("check comment");
  std::string arr[] = {"\ntest", "\ntest", ";test"};
  for ( const auto s : arr)
    {
      assert(Comment::isComment(s));
    }
  ok();
  std::cout << "\t\033[1;34mend check Comment\033[0m" << std::endl;
}

void ok()
{
  std::cout << "\033[1;32mok\033[0m" << std::endl;
}

void info(const char *msg)
{
  std::cout << msg << ": ";
}
