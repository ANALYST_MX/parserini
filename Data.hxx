#ifndef DATA_HXX
#define DATA_HXX

#include <string>
#include <list>
#include "Section.hxx"

class Data
{
public:
  Data();
  Data(const Data &);
  Data(const Section &);
  ~Data();
  void addSection(const Section &);
  Data &operator=(const Data &);
  void clear();
private:
  std::list<Section *> data;
};

#endif
