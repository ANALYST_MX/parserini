#include "Entry.hxx"

Entry::Entry()
{
}

Entry::Entry(const Pair &p)
{
  add(p);
}

Entry::Entry(const Entry &other)
{
  if ( this != &other )
    {
      container.assign(other.container.begin(), other.container.end());
    }
}

Entry::Entry(const std::string key, const std::string value)
{
  add(key, value);
}

Entry::~Entry()
{
  clear();
}

Entry Entry::operator()(const std::string key, const std::string value)
{
  Entry entry(key, value);
  return entry;
}

Entry &Entry::operator=(const Entry &other)
{
  if ( this != &other && &other != nullptr )
    {
      clear();
      container.assign(other.container.begin(), other.container.end());
    }
   
   return *this;
}

void Entry::add(const Pair &newPair)
{
  Pair *p = new Pair(newPair);
  container.push_front(p);
}

void Entry::add(const std::string key, const std::string value)
{
  Pair *p = new Pair(key, value);
  container.push_front(p);
}

void Entry::update(const std::string key, const std::string newValue)
{
  for ( auto it = container.begin(); it != container.end(); ++it )
    {
      
      if ( (*it)->getKey().compare(key) == 0 )
	{
	  (*it)->setValue(newValue);
	}
    }
}

void Entry::clear()
{
  container.clear();
}
