#include "Pair.hxx"

Pair::Pair(const std::string newKey, const std::string newValue)
  : value(newValue)
{
  setKey(newKey);
}

Pair::Pair(const Pair &other)
{
  if ( this != &other )
    {
      key = other.key;
      value = other.value;
    }
}

Pair::~Pair()
{
}

Pair &Pair::operator=(const Pair &other)
{
  if ( this != &other )
    {
      key = other.key;
      value = other.value;
    }
  
  return *this;
}

bool Pair::operator==(const Pair &other) const
{
  if ( this == &other )
    {
      return true;
    }
  
  if ( (key.compare(other.key) == 0) && (value.compare(other.value) == 0) )
    {
      return true;
    }

  return false;
}

bool Pair::operator!=(const Pair &other) const
{
  return !(*this == other);
}

std::string Pair::getKey() const
{
  return key;
}

std::string Pair::getValue() const
{
  return value;
}

void Pair::setKey(const std::string newKey)
{
  key = newKey;
  boost::algorithm::trim(key);
}

void Pair::setValue(const std::string newValue)
{
  value = newValue;
}
