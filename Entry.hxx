#ifndef ENTRY_HXX
#define ENTRY_HXX

#include <string>
#include <forward_list>
#include <boost/algorithm/string.hpp>
#include "Pair.hxx"

class Entry
{
public:
  Entry();
  Entry(const Pair &);
  Entry(const Entry &);
  Entry(const std::string, const std::string = std::string());
  virtual ~Entry();
  virtual Entry operator()(const std::string, const std::string = std::string());
  virtual Entry &operator=(const Entry &);  
  virtual void add(const Pair &);
  virtual void add(const std::string, const std::string = std::string());
  virtual void update(const std::string, const std::string);
  virtual void clear();
protected:
  std::forward_list<Pair *> container;
};

#endif
